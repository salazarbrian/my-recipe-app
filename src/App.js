import './App.css';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import React, {useState,useEffect} from 'react';
import Search from './Search';

function App() {
  const appId = "1349a216"
  const appKey =  "de8ba08ea11962f932ace5a0d5848ae7"

  const [recipe, setRecipe] = useState([]);
  const [search, setSearch] = useState("")
  const [send, setSend] = useState("")

  useEffect(() => {
  getRecipe()
  },[send])
console.log(send)

//gumagana
  const getRecipe = async() =>{
   await fetch(`https://api.edamam.com/search?q=${send}&app_id=${appId}&app_key=${appKey}`)
  .then(res => res.json())
  .then(data => {
  console.log(data)
  setRecipe(data.hits)})
 }  


  const submitSearch = e =>{
    setSearch(e.target.value)
  }

  const submitSend = e =>{
    e.preventDefault()
    setSend(search)
  }

  console.log(search)

  return (
    <div className="App">
    <h1 className="header">My Recipe</h1>
      <Form onSubmit={submitSend} className="search-form">
        <input
        className="search-bar" 
        type="text"
        value={search}
        onChange={submitSearch}/>
        <Button variant="primary" className="btn btn-primary" type="submit">
          Submit
        </Button>
      </Form>
      <div className="recipes">
      {recipe.map(data => 

        (<Search
        key= {data.recipe.label}
        label = {data.recipe.label}
        image = {data.recipe.image}
        cuisineType = {data.recipe.cuisineType}
        calories = {data.recipe.calories.toFixed(2)}
        ingredients = {data.recipe.ingredients} />
        ))}
      </div>
    </div>
  );
}

export default App;
