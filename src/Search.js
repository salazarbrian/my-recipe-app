import React from 'react';
import style from './recipe.module.css'

export default function Search({label,image,dishType,cuisineType,calories,ingredients}) {

	return(

		<div className={style.recipe}>
			<h1 className="name">{label}</h1>
			<h3>{cuisineType}</h3>
			<h3>{dishType}</h3>
					<ol>
				{ingredients.map(ingredient =>(
						<li className="list">{ingredient.text}</li>
					))}
			</ol>
			<h3>Calories</h3><h4>{calories}</h4>
			<img className="image" src={image} alt="" />
		</div>
		)
}
